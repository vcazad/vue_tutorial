import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import About from '@/components/About'
import Contact from '@/components/ContactUs'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
        path :'/about-us',
        name : 'AboutWord',
        component : About

    },
     {
        path :'/contact-us',
        name : 'ContactUs',
        component : Contact

    }
  ]
})
